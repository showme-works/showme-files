import { LitElement, html } from '@polymer/lit-element';
import git from 'isomorphic-git';
import browserfs from 'browserfs';

class ShowmeFiles extends LitElement {
  
  constructor() {
    super();
     browserfs.configure({ fs: 'IndexedDB', options: {}}, err => {
      if (err) return console.log(err);
      const fs = browserfs.BFSRequire("fs");
      //TODO Maybe we shouldn't get rid of everything that came before?
      fs.getRootFS().empty();
      git.plugins.set('fs', fs);
//       console.dir(fs);
//       console.dir(git);
//       const pfs = pify(fs);
//       await pfs.mkdir('test');
//       await git.clone({
//         'test',
//         corsProxy: 'https://cors.isomorphic-git.org',
//         url: 'https://github.com/isomorphic-git/isomorphic-git',
//         ref: 'master',
//         singleBranch: true,
//         depth: 10
//       });
//       await pfs.readdir('test');
    });
  }
  render() {
    return html`<div>Hello world.</div>`;
  }
}
customElements.define('showme-files', ShowmeFiles);