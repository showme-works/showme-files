import resolve from 'rollup-plugin-node-resolve';
import common from 'rollup-plugin-commonjs';
import builtins from 'rollup-plugin-node-builtins';
import globals from 'rollup-plugin-node-globals';
import babel from 'rollup-plugin-babel';
export default {
  input: 'src/index.js',
  external: ['isomorphic-git', 'browserfs'],
  output: {
    file: 'dist/bundle.js',
    format: 'esm',
    paths: {
      'isomorphic-git': 'https://dev.jspm.io/isomorphic-git',
      'browserfs':  'https://dev.jspm.io/browserfs'
    }
  },
  plugins: [
    builtins({
      preferBuiltins: true
    }),
    globals(),
    resolve({
      jsxnext: true,
      module: true,
      browser: true
    }),
    common(),
    babel({
      exclude: 'node_modules/**'
    })
  ]
};